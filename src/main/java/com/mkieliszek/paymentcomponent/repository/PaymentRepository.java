package com.mkieliszek.paymentcomponent.repository;

import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import java.util.List;
import java.util.Optional;

public interface PaymentRepository {

    Optional<PaymentDb> findById(String id);

    void deleteById(String id);

    List<PaymentDb> findAll();

    PaymentDb save(PaymentDb payment);
}
