package com.mkieliszek.paymentcomponent.model.entity;

import com.mkieliszek.paymentcomponent.model.AccountNumber;
import com.mkieliszek.paymentcomponent.model.Amount;
import com.mkieliszek.paymentcomponent.service.data.Payment;
import com.mkieliszek.paymentcomponent.service.data.PaymentUpdateParams;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@Entity
@Table(name = "payment")
@Builder
@Getter
@With
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PaymentDb {

    @Id
    private String id;

    @Embedded
    private Amount amount;

    private String currency;

    private String userId;

    @Embedded
    private AccountNumber targetBankAccNumber;

    public PaymentDb update(PaymentUpdateParams params) {
        return PaymentDb.builder()
                .id(getId())
                .amount(new Amount(params.getAmount()))
                .currency(params.getCurrency())
                .userId(params.getUserId())
                .targetBankAccNumber(new AccountNumber(params.getTargetBankAccNumber()))
                .build();
    }

    public Payment toPayment() {
        return Payment.builder()
                .id(id)
                .amount(amount.getAmount())
                .currency(currency)
                .userId(userId)
                .targetBankAccNumber(targetBankAccNumber.getAccountNumber())
                .build();
    }
}
