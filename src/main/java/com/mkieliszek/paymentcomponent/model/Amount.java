package com.mkieliszek.paymentcomponent.model;

import static java.util.Objects.isNull;

import com.mkieliszek.paymentcomponent.exception.ValidationException;
import java.math.BigDecimal;
import javax.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor
public class Amount {

    private static final String VALIDATION_MESSAGE = "Amount: %s. Must be greater than zero";

    private BigDecimal amount;

    public Amount(BigDecimal amount) {
        validateAmount(amount);
        this.amount = amount;
    }

    private void validateAmount(BigDecimal amount) {
        //complicated amount validation...
        if (isNull(amount) || !isGreaterThanZero(amount)) {
            throw new ValidationException(String.format(VALIDATION_MESSAGE, amount));
        }
    }

    private boolean isGreaterThanZero(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) > 0;
    }
}
