package com.mkieliszek.paymentcomponent.model;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNumeric;

import com.mkieliszek.paymentcomponent.exception.ValidationException;
import javax.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor
public class AccountNumber {

    private static final String VALIDATION_MESSAGE = "Account number: %s, is not valid";

    private String accountNumber;

    public AccountNumber(String accountNumber) {
        validateAccNumber(accountNumber);
        this.accountNumber = accountNumber;
    }

    private void validateAccNumber(String accountNumber) {
        //complicated account number validation...
        if (isBlank(accountNumber) || !isNumeric(accountNumber)) {
            throw new ValidationException(String.format(VALIDATION_MESSAGE, accountNumber));
        }
    }
}
