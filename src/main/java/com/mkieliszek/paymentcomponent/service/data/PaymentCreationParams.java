package com.mkieliszek.paymentcomponent.service.data;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PaymentCreationParams {

    BigDecimal amount;

    String currency;

    String userId;

    String targetBankAccNumber;
}
