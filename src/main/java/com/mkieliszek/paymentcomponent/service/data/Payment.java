package com.mkieliszek.paymentcomponent.service.data;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Payment {

    String id;

    BigDecimal amount;

    String currency;

    String userId;

    String targetBankAccNumber;
}
