package com.mkieliszek.paymentcomponent.service;

import com.mkieliszek.paymentcomponent.model.AccountNumber;
import com.mkieliszek.paymentcomponent.model.Amount;
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import com.mkieliszek.paymentcomponent.service.data.PaymentCreationParams;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
class PaymentFactory {

    public PaymentDb create(PaymentCreationParams creationParams) {
        return PaymentDb.builder()
                .id(UUID.randomUUID().toString())
                .amount(new Amount(creationParams.getAmount()))
                .currency(creationParams.getCurrency())
                .userId(creationParams.getUserId())
                .targetBankAccNumber(new AccountNumber(creationParams.getTargetBankAccNumber()))
                .build();
    }

}
