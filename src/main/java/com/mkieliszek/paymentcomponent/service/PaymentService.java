package com.mkieliszek.paymentcomponent.service;

import com.mkieliszek.paymentcomponent.exception.ResourceNotFoundException;
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import com.mkieliszek.paymentcomponent.repository.PaymentRepository;
import com.mkieliszek.paymentcomponent.service.data.Payment;
import com.mkieliszek.paymentcomponent.service.data.PaymentCreationParams;
import com.mkieliszek.paymentcomponent.service.data.PaymentUpdateParams;
import io.vavr.control.Option;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final PaymentFactory paymentFactory;

    public Payment getPaymentById(String id) {
        log.info("Getting payment with id: {}", id);
        return paymentRepository.findById(id)
                .map(PaymentDb::toPayment)
                .orElseThrow(() -> new ResourceNotFoundException(id));
    }

    public Payment createPayment(PaymentCreationParams params) {
        log.info("Creating payment: {}", params);
        var savedPayment = paymentRepository.save(paymentFactory.create(params));
        log.info("Payment was saved with id: {}", savedPayment.getId());
        return savedPayment.toPayment();
    }

    public Payment updatePayment(String id, PaymentUpdateParams params) {
        log.info("Updating payment with id: {} and data: {}", id, params);
        return Option.ofOptional(paymentRepository.findById(id))
                .map(payment -> payment.update(params))
                .map(paymentRepository::save)
                .map(PaymentDb::toPayment)
                .peek(p -> log.info("Payment updated with id: {}", p.getId()))
                .getOrElseThrow(() -> new ResourceNotFoundException(id));
    }

    public List<Payment> getListOfPayments() {
        log.info("Getting list of payments");
        return paymentRepository.findAll()
                .stream()
                .map(PaymentDb::toPayment)
                .collect(Collectors.toList());
    }

    public void deletePayment(String id) {
        log.info("Deleting payment with id: {}", id);
        paymentRepository.deleteById(id);
    }

}
