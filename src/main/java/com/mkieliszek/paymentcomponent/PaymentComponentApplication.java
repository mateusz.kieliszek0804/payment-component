package com.mkieliszek.paymentcomponent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentComponentApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentComponentApplication.class, args);
    }

}
