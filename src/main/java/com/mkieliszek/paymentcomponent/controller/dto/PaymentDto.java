package com.mkieliszek.paymentcomponent.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class PaymentDto {

    @JsonProperty("id")
    private final String id;

    @JsonProperty("amount")
    private final BigDecimal amount;

    @JsonProperty("currency")
    private final String currency;

    @JsonProperty("userId")
    private final String userId;

    @JsonProperty("targetBankAccNumber")
    private final String targetBankAccNumber;
}
