package com.mkieliszek.paymentcomponent.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePaymentDto {

    @JsonProperty(value = "amount", required = true)
    private BigDecimal amount;

    @JsonProperty(value = "currency", required = true)
    private String currency;

    @JsonProperty(value = "userId", required = true)
    private String userId;

    @JsonProperty(value = "targetBankAccNumber", required = true)
    private String targetBankAccNumber;
}
