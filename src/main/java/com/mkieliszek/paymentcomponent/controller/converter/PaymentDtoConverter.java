package com.mkieliszek.paymentcomponent.controller.converter;

import com.mkieliszek.paymentcomponent.controller.dto.CreatePaymentDto;
import com.mkieliszek.paymentcomponent.controller.dto.PaymentDto;
import com.mkieliszek.paymentcomponent.controller.dto.UpdatePaymentDto;
import com.mkieliszek.paymentcomponent.service.data.Payment;
import com.mkieliszek.paymentcomponent.service.data.PaymentCreationParams;
import com.mkieliszek.paymentcomponent.service.data.PaymentUpdateParams;
import org.springframework.stereotype.Component;

@Component
public class PaymentDtoConverter {

    public PaymentCreationParams toPaymentCreationParams(CreatePaymentDto dto) {
        return PaymentCreationParams.builder()
                .amount(dto.getAmount())
                .currency(dto.getCurrency())
                .userId(dto.getUserId())
                .targetBankAccNumber(dto.getTargetBankAccNumber())
                .build();
    }

    public PaymentUpdateParams toPaymentUpdateParams(UpdatePaymentDto dto) {
        return PaymentUpdateParams.builder()
                .amount(dto.getAmount())
                .currency(dto.getCurrency())
                .userId(dto.getUserId())
                .targetBankAccNumber(dto.getTargetBankAccNumber())
                .build();
    }

    public PaymentDto toPaymentDto(Payment payment) {
        return PaymentDto.builder()
                .id(payment.getId())
                .amount(payment.getAmount())
                .currency(payment.getCurrency())
                .userId(payment.getUserId())
                .targetBankAccNumber(payment.getTargetBankAccNumber())
                .build();
    }
}
