package com.mkieliszek.paymentcomponent.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import com.mkieliszek.paymentcomponent.controller.converter.PaymentDtoConverter;
import com.mkieliszek.paymentcomponent.controller.dto.CreatePaymentDto;
import com.mkieliszek.paymentcomponent.controller.dto.PaymentDto;
import com.mkieliszek.paymentcomponent.controller.dto.UpdatePaymentDto;
import com.mkieliszek.paymentcomponent.service.PaymentService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/payment")
@AllArgsConstructor
class PaymentControllerV1 {

    private final PaymentService paymentService;
    private final PaymentDtoConverter converter;

    @GetMapping("/{id}")
    ResponseEntity<PaymentDto> getPayment(@PathVariable String id) {
        var payment = paymentService.getPaymentById(id);
        return ResponseEntity.ok(converter.toPaymentDto(payment));
    }

    @GetMapping
    ResponseEntity<List<PaymentDto>> getAllPayments() {
        var payments = paymentService.getListOfPayments();
        return ResponseEntity.ok(payments.stream()
                .map(converter::toPaymentDto)
                .collect(Collectors.toList()));
    }

    @PostMapping
    ResponseEntity<PaymentDto> createPayment(@RequestBody CreatePaymentDto dto) {
        var creationParams = converter.toPaymentCreationParams(dto);
        var createdPayment = paymentService.createPayment(creationParams);
        return new ResponseEntity<>(converter.toPaymentDto(createdPayment), CREATED);
    }

    @PutMapping("/{id}")
    ResponseEntity<PaymentDto> updatePayment(@PathVariable String id, @RequestBody UpdatePaymentDto dto) {
        var updateParams = converter.toPaymentUpdateParams(dto);
        var updatedPayment = paymentService.updatePayment(id, updateParams);
        return new ResponseEntity<>(converter.toPaymentDto(updatedPayment), OK);
    }

    @DeleteMapping("/{id}")
    void deleteSmsTemplate(@PathVariable String id) {
        paymentService.deletePayment(id);
    }

}
