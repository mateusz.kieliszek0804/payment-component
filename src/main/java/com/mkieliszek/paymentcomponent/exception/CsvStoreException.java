package com.mkieliszek.paymentcomponent.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CsvStoreException extends RuntimeException {

    public CsvStoreException() {
        super("Error during operation on csv store");
    }

}
