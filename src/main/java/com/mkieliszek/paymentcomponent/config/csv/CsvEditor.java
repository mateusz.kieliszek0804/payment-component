package com.mkieliszek.paymentcomponent.config.csv;

import static com.mkieliszek.paymentcomponent.config.csv.CsvRowConverter.getCsvLineFromPayment;
import static com.mkieliszek.paymentcomponent.config.csv.CsvRowConverter.getPaymentFromCsvLine;
import static com.opencsv.ICSVWriter.DEFAULT_LINE_END;
import static com.opencsv.ICSVWriter.DEFAULT_SEPARATOR;
import static com.opencsv.ICSVWriter.NO_ESCAPE_CHARACTER;
import static com.opencsv.ICSVWriter.NO_QUOTE_CHARACTER;

import com.mkieliszek.paymentcomponent.exception.CsvStoreException;
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@ConditionalOnProperty(name = "datasource.type", havingValue = "csv")
class CsvEditor {

    private final String csvFile;

    CsvEditor(@Value("${csv.reader.file.path}") String csvFile) {
        this.csvFile = csvFile;
    }

    void deletePayment(String id) {
        List<PaymentDb> paymentsToSave = getAllPayments().stream()
                .filter(payment -> !payment.getId().equals(id))
                .collect(Collectors.toList());
        try (CSVWriter writer = new CSVWriter(new FileWriter(csvFile),
                DEFAULT_SEPARATOR, NO_QUOTE_CHARACTER, NO_ESCAPE_CHARACTER, DEFAULT_LINE_END)) {
            paymentsToSave.forEach(payment -> writer.writeNext(getCsvLineFromPayment(payment)));
        } catch (IOException e) {
            throw new CsvStoreException();
        }
    }

    PaymentDb createPayment(PaymentDb payment) {
        Optional<PaymentDb> paymentById = getPaymentById(payment.getId());
        if(paymentById.isPresent()) {
            updatePayment(payment);
        } else {
            try (CSVWriter writer = new CSVWriter(new FileWriter(csvFile, true),
                    DEFAULT_SEPARATOR, NO_QUOTE_CHARACTER, NO_ESCAPE_CHARACTER, DEFAULT_LINE_END)) {
                writer.writeNext(getCsvLineFromPayment(payment));
            } catch (IOException e) {
                throw new CsvStoreException();
            }
        }
        return payment;
    }

    private void updatePayment(PaymentDb paymentToUpdate) {
        List<PaymentDb> paymentsToSave = getAllPayments().stream()
                .filter(payment -> !payment.getId().equals(paymentToUpdate.getId()))
                .collect(Collectors.toList());
        paymentsToSave.add(paymentToUpdate);

        try (CSVWriter writer = new CSVWriter(new FileWriter(csvFile), DEFAULT_SEPARATOR,
                NO_QUOTE_CHARACTER, NO_ESCAPE_CHARACTER, DEFAULT_LINE_END)) {
            paymentsToSave.forEach(payment -> writer.writeNext(getCsvLineFromPayment(payment)));
        } catch (IOException e) {
            throw new CsvStoreException();
        }
    }

    List<PaymentDb> getAllPayments() {
        try (CSVReader reader = new CSVReader(new FileReader(csvFile))) {
            List<PaymentDb> payments = new ArrayList<>();
            String[] line;
            while ((line = reader.readNext()) != null) {
                payments.add(getPaymentFromCsvLine(line));
            }
            return payments;
        } catch (IOException | CsvValidationException e) {
          throw new CsvStoreException();
        }
    }

    Optional<PaymentDb> getPaymentById(String id) {
        try (CSVReader reader = new CSVReader(new FileReader(csvFile))) {
            String[] line;
            while ((line = reader.readNext()) != null) {
                String idFromCsv = line[0];
                if (id.equals(idFromCsv)) {
                    return Optional.of(getPaymentFromCsvLine(line));
                }
            }
            return Optional.empty();
        } catch (IOException | CsvValidationException e) {
            throw new CsvStoreException();
        }
    }

}
