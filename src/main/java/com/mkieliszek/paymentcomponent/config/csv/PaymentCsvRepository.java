package com.mkieliszek.paymentcomponent.config.csv;

import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import com.mkieliszek.paymentcomponent.repository.PaymentRepository;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
@ConditionalOnProperty(name = "datasource.type", havingValue = "csv")
class PaymentCsvRepository implements PaymentRepository {

    private final CsvEditor csvEditor;

    @Override
    public Optional<PaymentDb> findById(String id) {
        return csvEditor.getPaymentById(id);
    }

    @Override
    public void deleteById(String id) {
        csvEditor.deletePayment(id);
    }

    @Override
    public List<PaymentDb> findAll() {
        return csvEditor.getAllPayments();
    }

    @Override
    public PaymentDb save(PaymentDb payment) {
        return csvEditor.createPayment(payment);
    }
}
