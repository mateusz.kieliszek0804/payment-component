package com.mkieliszek.paymentcomponent.config.csv;

import com.mkieliszek.paymentcomponent.model.AccountNumber;
import com.mkieliszek.paymentcomponent.model.Amount;
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import java.math.BigDecimal;
import lombok.experimental.UtilityClass;

@UtilityClass
class CsvRowConverter {

    static PaymentDb getPaymentFromCsvLine(String[] line) {
        return PaymentDb.builder()
                .id(line[0])
                .amount(new Amount(new BigDecimal(line[1])))
                .currency(line[2])
                .userId(line[3])
                .targetBankAccNumber(new AccountNumber(line[4]))
                .build();
    }

    static String[] getCsvLineFromPayment(PaymentDb payment) {
        return new String[]{
                payment.getId(),
                payment.getAmount().getAmount().toString(),
                payment.getCurrency(),
                payment.getUserId(),
                payment.getTargetBankAccNumber().getAccountNumber()
        };
    }

}
