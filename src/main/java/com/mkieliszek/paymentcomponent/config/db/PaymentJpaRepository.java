package com.mkieliszek.paymentcomponent.config.db;

import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;

@ConditionalOnProperty(name = "datasource.type", havingValue = "db")
interface PaymentJpaRepository extends JpaRepository<PaymentDb, String> {

}
