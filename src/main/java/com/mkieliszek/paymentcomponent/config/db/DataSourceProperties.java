package com.mkieliszek.paymentcomponent.config.db;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(name = "datasource.type", havingValue = "db")
@ConfigurationProperties(prefix = "db")
@Getter
@Setter
public class DataSourceProperties {

    private String driver;

    private String url;

    private String username;

    private String password;

}
