package com.mkieliszek.paymentcomponent.config.db;

import com.mkieliszek.paymentcomponent.model.entity.PaymentDb;
import com.mkieliszek.paymentcomponent.repository.PaymentRepository;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

@Repository
@ConditionalOnProperty(name = "datasource.type", havingValue = "db")
@AllArgsConstructor
public class PaymentDbRepository implements PaymentRepository {

    private final PaymentJpaRepository paymentJpaRepository;

    @Override
    public Optional<PaymentDb> findById(String id) {
        return paymentJpaRepository.findById(id);
    }

    @Override
    public void deleteById(String id) {
        paymentJpaRepository.deleteById(id);
    }

    @Override
    public List<PaymentDb> findAll() {
        return paymentJpaRepository.findAll();
    }

    @Override
    public PaymentDb save(PaymentDb payment) {
        return paymentJpaRepository.save(payment);
    }
}
