package com.mkieliszek.paymentcomponent.service

import com.mkieliszek.paymentcomponent.exception.ResourceNotFoundException
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb
import com.mkieliszek.paymentcomponent.repository.PaymentRepository
import spock.lang.Specification
import spock.lang.Subject

import static com.mkieliszek.paymentcomponent.model.entity.PaymentDbFixture.somePaymentDb
import static com.mkieliszek.paymentcomponent.service.data.PaymentCreationParamsFixture.somePaymentCreationParams
import static com.mkieliszek.paymentcomponent.service.data.PaymentUpdateParamsFixture.somePaymentUpdateParams

class PaymentServiceSpec extends Specification {

    PaymentRepository paymentRepository = Mock()
    PaymentFactory paymentFactory = new PaymentFactory()

    @Subject
    def paymentService = new PaymentService(paymentRepository, paymentFactory)

    def 'should get payment by id'() {
        given:
        def id = UUID.randomUUID().toString()
        def expectedPayment = somePaymentDb()

        and:
        paymentRepository.findById(id) >> Optional.of(expectedPayment)

        when:
        def result = paymentService.getPaymentById(id)

        then:
        result == expectedPayment.toPayment()
    }

    def 'should throw exception when payment was not found'() {
        given:
        def id = UUID.randomUUID().toString()

        and:
        paymentRepository.findById(id) >> Optional.empty()

        when:
        paymentService.getPaymentById(id)

        then:
        def exception = thrown(ResourceNotFoundException)
        exception.message == String.format('Resource with id: %s not found', id)
    }

    def 'should create payment'() {
        given:
        def creationParams = somePaymentCreationParams()
        def savedPayment = somePaymentDb()

        when:
        def result = paymentService.createPayment(creationParams)

        then:
        1 * paymentRepository.save(_ as PaymentDb) >> savedPayment
        result == savedPayment.toPayment()
    }

    def 'should update payment'() {
        given:
        def paymentBeforeUpdate = somePaymentDb()
        def id = paymentBeforeUpdate.getId()

        and:
        def updateParams = somePaymentUpdateParams()
        def expectedPayment = paymentBeforeUpdate.update(updateParams)

        when:
        def result = paymentService.updatePayment(id, updateParams)

        then:
        1 * paymentRepository.findById(id) >> Optional.of(paymentBeforeUpdate)
        1 * paymentRepository.save(expectedPayment) >> expectedPayment
        result == expectedPayment.toPayment()
    }

    def 'should throw exception when trying update payment but it dont exist'() {
        given:
        def id = UUID.randomUUID().toString()

        paymentRepository.findById(id) >> Optional.empty()

        when:
        paymentService.updatePayment(id, somePaymentUpdateParams())

        then:
        def exception = thrown(ResourceNotFoundException)
        exception.message == 'Resource with id: ' + id + ' not found'
    }

    def 'should return list of payments'() {
        given:
        def firstPayment = somePaymentDb()
        def secondPayment = somePaymentDb()
        paymentRepository.findAll() >> [firstPayment, secondPayment]

        when:
        def result = paymentService.getListOfPayments()

        then:
        result == [firstPayment.toPayment(), secondPayment.toPayment()]
    }

    def 'should delete payment'() {
        given:
        def id = UUID.randomUUID().toString()

        when:
        paymentService.deletePayment(id)

        then:
        1 * paymentRepository.deleteById(id)
    }
}
