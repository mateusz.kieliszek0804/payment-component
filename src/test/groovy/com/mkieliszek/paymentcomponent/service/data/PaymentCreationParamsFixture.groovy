package com.mkieliszek.paymentcomponent.service.data

import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.RandomUtils

class PaymentCreationParamsFixture {

    static PaymentCreationParams somePaymentCreationParams() {
        return PaymentCreationParams.builder()
                .amount(new BigDecimal(BigInteger.valueOf(new Random().nextInt(RandomUtils.nextInt())), 2))
                .currency(RandomStringUtils.randomAlphabetic(3).toUpperCase())
                .userId(UUID.randomUUID().toString())
                .targetBankAccNumber(RandomStringUtils.randomNumeric(12))
                .build()
    }
}
