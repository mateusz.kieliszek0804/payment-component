package com.mkieliszek.paymentcomponent.service

import com.mkieliszek.paymentcomponent.model.AccountNumber
import com.mkieliszek.paymentcomponent.model.Amount
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb
import spock.lang.Specification
import spock.lang.Subject

import static com.mkieliszek.paymentcomponent.service.data.PaymentCreationParamsFixture.somePaymentCreationParams

class PaymentFactorySpec extends Specification {

    @Subject
    def factory = new PaymentFactory()

    def 'should create PaymentDb from PaymentCreationParams'() {
        given:
        def creationParams = somePaymentCreationParams()

        when:
        def result = factory.create(creationParams)

        then:
        result == PaymentDb.builder()
                .id(result.id)
                .amount(new Amount(creationParams.getAmount()))
                .currency(creationParams.getCurrency())
                .userId(creationParams.getUserId())
                .targetBankAccNumber(new AccountNumber(creationParams.getTargetBankAccNumber()))
                .build()
    }
}
