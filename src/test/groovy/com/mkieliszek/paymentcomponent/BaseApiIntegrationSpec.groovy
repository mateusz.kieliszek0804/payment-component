package com.mkieliszek.paymentcomponent

import io.restassured.RestAssured
import org.springframework.beans.factory.annotation.Value

class BaseApiIntegrationSpec extends BaseIntegrationSpec {

    @Value('${local.server.port}')
    private int port

    def setup() {
        RestAssured.reset()
        RestAssured.port = port

    }

}
