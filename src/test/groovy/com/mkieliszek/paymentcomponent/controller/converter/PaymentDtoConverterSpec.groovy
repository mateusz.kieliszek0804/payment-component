package com.mkieliszek.paymentcomponent.controller.converter

import com.mkieliszek.paymentcomponent.controller.dto.PaymentDto
import com.mkieliszek.paymentcomponent.service.data.PaymentCreationParams
import com.mkieliszek.paymentcomponent.service.data.PaymentUpdateParams
import spock.lang.Specification
import spock.lang.Subject

import static com.mkieliszek.paymentcomponent.controller.dto.CreatePaymentDtoFixture.someCreatePaymentDto
import static com.mkieliszek.paymentcomponent.controller.dto.UpdatePaymentDtoFixture.someUpdatePaymentDto
import static com.mkieliszek.paymentcomponent.service.data.PaymentFixture.somePayment

class PaymentDtoConverterSpec extends Specification {

    @Subject
    def converter = new PaymentDtoConverter()

    def 'should map CreatePaymentDto to PaymentCreationParams'() {
        given:
        def createPaymentDto = someCreatePaymentDto()

        when:
        def result = converter.toPaymentCreationParams(createPaymentDto)

        then:
        result == PaymentCreationParams.builder()
                .amount(createPaymentDto.getAmount())
                .currency(createPaymentDto.getCurrency())
                .userId(createPaymentDto.getUserId())
                .targetBankAccNumber(createPaymentDto.getTargetBankAccNumber())
                .build()
    }

    def 'should map UpdatePaymentDto to PaymentUpdateParams'() {
        given:
        def updatePaymentDto = someUpdatePaymentDto()

        when:
        def result = converter.toPaymentUpdateParams(updatePaymentDto)

        then:
        result == PaymentUpdateParams.builder()
                .amount(updatePaymentDto.getAmount())
                .currency(updatePaymentDto.getCurrency())
                .userId(updatePaymentDto.getUserId())
                .targetBankAccNumber(updatePaymentDto.getTargetBankAccNumber())
                .build()
    }

    def 'should map Payment to PaymentDto'() {
        given:
        def payment = somePayment()
        when:
        def result = converter.toPaymentDto(payment)
        then:
        result == PaymentDto.builder()
                .id(payment.getId())
                .amount(payment.getAmount())
                .currency(payment.getCurrency())
                .userId(payment.getUserId())
                .targetBankAccNumber(payment.getTargetBankAccNumber())
                .build()
    }
}
