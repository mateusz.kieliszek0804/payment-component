package com.mkieliszek.paymentcomponent.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.mkieliszek.paymentcomponent.BaseApiIntegrationSpec
import com.mkieliszek.paymentcomponent.repository.PaymentRepository
import io.restassured.http.ContentType
import org.apache.http.HttpStatus
import org.springframework.beans.factory.annotation.Autowired

import static com.mkieliszek.paymentcomponent.controller.dto.CreatePaymentDtoFixture.someCreatePaymentDto
import static com.mkieliszek.paymentcomponent.controller.dto.UpdatePaymentDtoFixture.someUpdatePaymentDto
import static com.mkieliszek.paymentcomponent.model.entity.PaymentDbFixture.somePaymentDb
import static io.restassured.RestAssured.given
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.notNullValue

class PaymentControllerV1IntegrationSpec extends BaseApiIntegrationSpec {

    @Autowired
    PaymentRepository paymentRepository

    def 'should get payment by id'() {
        given:
        def payment = somePaymentDb()
        paymentRepository.save(payment)

        def request = given()
                .log().all()
                .pathParam("id", payment.id)

        when:
        def response = request.get('/v1/payment/{id}')

        then:
        response.then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body('id', equalTo(payment.id))
                .body('currency', equalTo(payment.currency))
                .body('userId', equalTo(payment.userId))
                .body('targetBankAccNumber', equalTo(payment.targetBankAccNumber.accountNumber))
    }

    def 'should return 404 when payment not found'() {
        given:
        def request = given()
                .log().all()
                .pathParam("id", UUID.randomUUID().toString())

        when:
        def response = request.get('/v1/payment/{id}')

        then:
        response.then()
                .log().all()
                .statusCode(HttpStatus.SC_NOT_FOUND)
    }

    def 'should list payments'() {
        given:
        def payment = somePaymentDb()
        paymentRepository.save(payment)

        def request = given().log().all()

        when:
        def response = request.get('/v1/payment')

        then:
        response.then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body('size()', equalTo(1))
                .body('[0].id', equalTo(payment.id))
                .body('[0].currency', equalTo(payment.currency))
                .body('[0].userId', equalTo(payment.userId))
                .body('[0].targetBankAccNumber', equalTo(payment.targetBankAccNumber.accountNumber))    }

    def 'should create payment'() {
        given:
        def someCreatePayment = someCreatePaymentDto()
        def request = given().log().all()
                .contentType(ContentType.JSON)
                .body(new ObjectMapper().writeValueAsString(someCreatePayment))

        when:
        def response = request.post('/v1/payment')

        then:
        response.then()
                .log().all()
                .statusCode(HttpStatus.SC_CREATED)
                .body('id', notNullValue())
                .body('currency', equalTo(someCreatePayment.currency))
                .body('userId', equalTo(someCreatePayment.userId))
                .body('targetBankAccNumber', equalTo(someCreatePayment.targetBankAccNumber))
    }

    def 'should update payment'() {
        given:
        def payment = somePaymentDb()
        paymentRepository.save(payment)

        def someUpdatePayment = someUpdatePaymentDto()
        def request = given().log().all()
                .contentType(ContentType.JSON)
                .body(new ObjectMapper().writeValueAsString(someUpdatePayment))
                .pathParam('id', payment.id)

        when:
        def response = request.put('/v1/payment/{id}')

        then:
        response.then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
                .body('id', equalTo(payment.id))
                .body('currency', equalTo(someUpdatePayment.currency))
                .body('userId', equalTo(someUpdatePayment.userId))
                .body('targetBankAccNumber', equalTo(someUpdatePayment.targetBankAccNumber))
    }

    def 'should delete payment by id'() {
        given:
        def payment = somePaymentDb()
        paymentRepository.save(payment)

        def request = given().log().all().pathParam("id", payment.id)

        when:
        def response = request.delete('/v1/payment/{id}')

        then:
        response.then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)
    }

}
