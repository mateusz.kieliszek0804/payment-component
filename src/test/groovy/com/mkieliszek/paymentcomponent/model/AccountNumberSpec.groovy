package com.mkieliszek.paymentcomponent.model

import com.mkieliszek.paymentcomponent.exception.ValidationException
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.StringUtils
import spock.lang.Specification
import spock.lang.Unroll

class AccountNumberSpec extends Specification {

    def 'should valid account number'() {
        given:
        def accountNumber = RandomStringUtils.randomNumeric(12)

        when:
        def result = new AccountNumber(accountNumber)

        then:
        noExceptionThrown()
        result.getAccountNumber() == accountNumber
    }

    @Unroll
    def 'should throw validation exception when account number is blank'() {
        when:
        new AccountNumber(accountNumber)

        then:
        def exception = thrown(expectedException)
        exception.message == String.format('Account number: %s, is not valid', accountNumber)

        where:
        accountNumber     || expectedException
        null              || ValidationException
        StringUtils.EMPTY || ValidationException
        StringUtils.SPACE || ValidationException
    }

    @Unroll
    def 'should throw validation exception when account number is not numeric'() {
        when:
        new AccountNumber(accountNumber)

        then:
        def exception = thrown(expectedException)
        exception.message == String.format('Account number: %s, is not valid', accountNumber)

        where:
        accountNumber                             || expectedException
        'abc'                                     || ValidationException
        'a1'                                      || ValidationException
        '1a'                                      || ValidationException
        RandomStringUtils.randomAlphanumeric(100) || ValidationException
    }
}
