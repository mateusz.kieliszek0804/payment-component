package com.mkieliszek.paymentcomponent.model.entity

import com.mkieliszek.paymentcomponent.model.AccountNumberFixture
import com.mkieliszek.paymentcomponent.model.AmountFixture
import org.apache.commons.lang3.RandomStringUtils

class PaymentDbFixture {

    static PaymentDb somePaymentDb() {
        return PaymentDb.builder()
        .id(UUID.randomUUID().toString())
        .amount(AmountFixture.someAmount())
        .currency(RandomStringUtils.randomAlphabetic(3).toUpperCase())
        .userId(UUID.randomUUID().toString())
        .targetBankAccNumber(AccountNumberFixture.someAccountNumber())
        .build()
    }
}
