package com.mkieliszek.paymentcomponent.model

import org.apache.commons.lang3.RandomUtils

class AmountFixture {

    static Amount someAmount() {
        return new Amount(new BigDecimal(BigInteger.valueOf(new Random().nextInt(RandomUtils.nextInt())), 2))
    }
}
