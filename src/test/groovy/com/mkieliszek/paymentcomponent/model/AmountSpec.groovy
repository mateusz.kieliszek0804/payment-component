package com.mkieliszek.paymentcomponent.model

import com.mkieliszek.paymentcomponent.exception.ValidationException
import spock.lang.Specification
import spock.lang.Unroll

class AmountSpec extends Specification {

    def 'should valid amount'() {
        given:
        def amount = new BigDecimal("12.34")

        when:
        def result = new Amount(amount)

        then:
        noExceptionThrown()
        result.getAmount() == amount
    }

    def 'should throw validation exception when amount is null'() {
        given:
        def amount = null

        when:
        new Amount(amount)

        then:
        def exception = thrown(ValidationException)
        exception.message == 'Amount: null. Must be greater than zero'
    }

    @Unroll
    def 'should throw validation exception when amount is not greater than zero'() {
        when:
        new Amount(amount)

        then:
        def exception = thrown(expectedException)
        exception.message == String.format('Amount: %s. Must be greater than zero', amount)

        where:
        amount                  || expectedException
        BigDecimal.ZERO         || ValidationException
        new BigDecimal("-1.00") || ValidationException
    }

}
