package com.mkieliszek.paymentcomponent.model

import org.apache.commons.lang3.RandomStringUtils

class AccountNumberFixture {

    static AccountNumber someAccountNumber() {
        return new AccountNumber(RandomStringUtils.randomNumeric(12))
    }
}
