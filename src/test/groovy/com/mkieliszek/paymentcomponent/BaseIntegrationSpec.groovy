package com.mkieliszek.paymentcomponent

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BaseIntegrationSpec extends Specification {

    @Autowired
    DatabaseCleaner dbCleaner

    def cleanup() {
        dbCleaner.cleanDb()
    }

}
