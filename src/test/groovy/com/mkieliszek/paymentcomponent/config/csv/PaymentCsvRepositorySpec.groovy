package com.mkieliszek.paymentcomponent.config.csv

import com.mkieliszek.paymentcomponent.model.entity.PaymentDbFixture
import spock.lang.Specification
import spock.lang.Subject

class PaymentCsvRepositorySpec extends Specification {

    def csvEditor = Mock(CsvEditor)

    @Subject
    def paymentCsvRepository = new PaymentCsvRepository(csvEditor)

    def "should return payment by id"() {
        given:
        def expectedPayment = PaymentDbFixture.somePaymentDb()

        when:
        def result = paymentCsvRepository.findById(expectedPayment.getId())

        then:
        1 * csvEditor.getPaymentById(expectedPayment.getId()) >> Optional.of(expectedPayment)
        result == Optional.of(expectedPayment)
    }

    def "should return optional empty when payment not found"() {
        given:
        def paymentId = UUID.randomUUID().toString()

        when:
        def result = paymentCsvRepository.findById(paymentId)

        then:
        1 * csvEditor.getPaymentById(paymentId) >> Optional.empty()
        result == Optional.empty()
    }

    def "should delete payment by id"() {
        given:
        def paymentId = UUID.randomUUID().toString()

        when:
        paymentCsvRepository.deleteById(paymentId)

        then:
        1 * csvEditor.deletePayment(paymentId)
    }

    def "should find all payments"() {
        given:
        def expectedPayments = [PaymentDbFixture.somePaymentDb(), PaymentDbFixture.somePaymentDb()]

        when:
        def paymentList = paymentCsvRepository.findAll()

        then:
        1 * csvEditor.getAllPayments() >> expectedPayments
        paymentList == expectedPayments

    }

    def "should save payment"() {
        given:
        def paymentToSave = PaymentDbFixture.somePaymentDb()

        when:
        def savedPayment = paymentCsvRepository.save(paymentToSave)

        then:
        1 * csvEditor.createPayment(paymentToSave) >> paymentToSave
        savedPayment == paymentToSave
    }
}
