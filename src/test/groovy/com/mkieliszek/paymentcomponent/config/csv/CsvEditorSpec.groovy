package com.mkieliszek.paymentcomponent.config.csv

import com.mkieliszek.paymentcomponent.model.AccountNumber
import com.mkieliszek.paymentcomponent.model.Amount
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb
import org.apache.commons.lang3.StringUtils
import spock.lang.Specification
import spock.lang.Subject

import static com.mkieliszek.paymentcomponent.model.entity.PaymentDbFixture.somePaymentDb

class CsvEditorSpec extends Specification {

    private static final String TMP_FILE_PATH = "src/test/resources/tmpStore.csv"

    @Subject
    def editor = new CsvEditor(TMP_FILE_PATH)

    def cleanup() {
        clearTmpFile()
    }

    def setupSpec() {
        createTmpFile()
    }

    def cleanupSpec() {
        deleteTmpFile()
    }

    def 'should get all payments from csv file'() {
        given:
        def editor = new CsvEditor("src/test/resources/twoPayments.csv")

        when:
        def result = editor.getAllPayments()

        then:
        result.size() == 2
        result.get(0) == PaymentDb.builder()
                .id('02836938-8d58-49ed-9913-4ceb875893d0')
                .amount(new Amount(new BigDecimal("100")))
                .currency('PLN')
                .userId('1b9a628f-221a-4f4e-a777-248bd33cfb96')
                .targetBankAccNumber(new AccountNumber('1234567890'))
                .build()

        result.get(1) == PaymentDb.builder()
                .id('d4ef01d5-ba8d-4df8-847c-312f8fbae083')
                .amount(new Amount(new BigDecimal("39")))
                .currency('USD')
                .userId('679c18aa-a777-4c1c-8e3a-02df786d6d8d')
                .targetBankAccNumber(new AccountNumber('9876543210'))
                .build()
    }

    def 'should find payment by id from csv file'() {
        given:
        def editor = new CsvEditor("src/test/resources/twoPayments.csv")

        when:
        def result = editor.getPaymentById('02836938-8d58-49ed-9913-4ceb875893d0')

        then:
        result == Optional.of(PaymentDb.builder()
                .id('02836938-8d58-49ed-9913-4ceb875893d0')
                .amount(new Amount(new BigDecimal("100")))
                .currency('PLN')
                .userId('1b9a628f-221a-4f4e-a777-248bd33cfb96')
                .targetBankAccNumber(new AccountNumber('1234567890'))
                .build())
    }

    def 'should return optional empty when payment not found'() {
        given:
        def editor = new CsvEditor("src/test/resources/twoPayments.csv")

        when:
        def result = editor.getPaymentById(UUID.randomUUID().toString())

        then:
        result == Optional.empty()
    }

    def 'should save payment to csv file'() {
        given:
        def somePayment = somePaymentDb()

        when:
        def result = editor.createPayment(somePayment)

        then:
        result == somePayment

        and:
        def savedPayment = editor.getPaymentById(somePayment.getId())
        savedPayment == Optional.of(somePayment)
    }

    def 'should update payment in csv file'() {
        given:
        def somePayment = somePaymentDb()
        editor.createPayment(somePayment)

        and:
        def paymentToUpdate = somePaymentDb().withId(somePayment.getId())

        when:
        def result = editor.createPayment(paymentToUpdate)

        then:
        result == paymentToUpdate

        and:
        def updatedPayment = editor.getPaymentById(paymentToUpdate.getId())
        updatedPayment == Optional.of(paymentToUpdate)
    }

    def 'should delete payment by id'() {
        given:
        def somePayment = somePaymentDb()
        editor.createPayment(somePayment)

        when:
        editor.deletePayment(somePayment.getId())

        then:
        editor.getAllPayments().size() == 0
    }

    private static boolean deleteTmpFile() {
        new File(TMP_FILE_PATH).delete()
    }

    private static boolean createTmpFile() {
        new File(TMP_FILE_PATH).createNewFile()
    }

    private static void clearTmpFile() {
        def file = new File(TMP_FILE_PATH)
        def writer = new PrintWriter(file)
        writer.print(StringUtils.EMPTY)
        writer.close()
    }

}
