package com.mkieliszek.paymentcomponent.config.csv

import com.mkieliszek.paymentcomponent.model.AccountNumber
import com.mkieliszek.paymentcomponent.model.Amount
import com.mkieliszek.paymentcomponent.model.entity.PaymentDb
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.RandomUtils
import spock.lang.Specification

class CsvRowConverterSpec extends Specification {

    def "should create payment from csv line"() {
        given:
        def id = UUID.randomUUID().toString()
        def amount = RandomUtils.nextInt().toString()
        def currency = RandomStringUtils.randomAlphabetic(3)
        def userId = UUID.randomUUID().toString()
        def accNumber = RandomUtils.nextInt().toString()

        and:
        def line = [id, amount, currency, userId, accNumber] as String[]

        when:
        def result = CsvRowConverter.getPaymentFromCsvLine(line)

        then:
        result == PaymentDb.builder()
                .id(id)
                .amount(new Amount(new BigDecimal(amount)))
                .currency(currency)
                .userId(userId)
                .targetBankAccNumber(new AccountNumber(accNumber))
                .build()
    }

    def "should create csv line from payment"() {
        given:
        def id = UUID.randomUUID().toString()
        def amount = RandomUtils.nextInt().toString()
        def currency = RandomStringUtils.randomAlphabetic(3)
        def userId = UUID.randomUUID().toString()
        def accNumber = RandomUtils.nextInt().toString()

        and:
        def payment = PaymentDb.builder()
                .id(id)
                .amount(new Amount(new BigDecimal(amount)))
                .currency(currency)
                .userId(userId)
                .targetBankAccNumber(new AccountNumber(accNumber))
                .build()

        when:
        def result = CsvRowConverter.getCsvLineFromPayment(payment)

        then:
        result == [id, amount, currency, userId, accNumber] as String[]
    }
}
