package com.mkieliszek.paymentcomponent.config.db

import com.mkieliszek.paymentcomponent.model.entity.PaymentDbFixture
import spock.lang.Specification
import spock.lang.Subject

class PaymentDbRepositorySpec extends Specification {

    def jpaRepository = Mock(PaymentJpaRepository)

    @Subject
    def paymentDbRepository = new PaymentDbRepository(jpaRepository)

    def "should return payment by id"() {
        given:
        def expectedPayment = PaymentDbFixture.somePaymentDb()

        when:
        def result = paymentDbRepository.findById(expectedPayment.getId())

        then:
        1 * jpaRepository.findById(expectedPayment.getId()) >> Optional.of(expectedPayment)
        result == Optional.of(expectedPayment)
    }

    def "should return optional empty when payment not found"() {
        given:
        def paymentId = UUID.randomUUID().toString()

        when:
        def result = paymentDbRepository.findById(paymentId)

        then:
        1 * jpaRepository.findById(paymentId) >> Optional.empty()
        result == Optional.empty()
    }

    def "should delete payment by id"() {
        given:
        def paymentId = UUID.randomUUID().toString()

        when:
        paymentDbRepository.deleteById(paymentId)

        then:
        1 * jpaRepository.deleteById(paymentId)
    }

    def "should find all payments"() {
        given:
        def expectedPayments = [PaymentDbFixture.somePaymentDb(), PaymentDbFixture.somePaymentDb()]

        when:
        def paymentList = paymentDbRepository.findAll()

        then:
        1 * jpaRepository.findAll() >> expectedPayments
        paymentList == expectedPayments

    }

    def "should save payment"() {
        given:
        def paymentToSave = PaymentDbFixture.somePaymentDb()

        when:
        def savedPayment = paymentDbRepository.save(paymentToSave)

        then:
        1 * jpaRepository.save(paymentToSave) >> paymentToSave
        savedPayment == paymentToSave
    }

}
