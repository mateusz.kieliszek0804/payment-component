package com.mkieliszek.paymentcomponent

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component

@Component
class DatabaseCleaner {

    @Autowired
    private JdbcTemplate jdbcTemplate

    private static List<String> tableNames = [
            "payment"
    ]

    void cleanDb() {
        tableNames.forEach {
            jdbcTemplate.execute("DELETE FROM $it")
        }
    }

}
